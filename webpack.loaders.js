"use strict";
const config = require('./config');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = function (env) {

    const isProd = env === 'production';

    // Print global path for files
    console.log('sass global path::::', config.globalSCSS);

    const loaders = [
        {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [
                    {
                        loader: 'css-loader',
                        options: {
                            module: true,
                            localIdentName: '[local]',
                        }
                    },
                    'postcss-loader'
                ]
            })
        },
        {
            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [
                    {
                        loader: 'css-loader',
                        options: {
                            module: true,
                            sourceMap: true,
                            importLoaders: 2,
                            minimize: isProd,
                            localIdentName: '[local]'
                        }
                    },
                    'sass-loader',
                    {
                        loader: 'sass-resources-loader',
                        options: {
                            resources: config.globalSCSS
                        }
                    }
                ]
            }),
        },
        {
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: ['babel-loader'],
        }];

    return loaders;

};

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import style from './_button.scss';

//
// Component stateless
const Button = props => {
    const {
        label,
        size,
        brand,
        disabled,
        onClick
    } = props;

    const sizeClass = size || 'medium';
    const brandClass = brand || 'lmt';

    // Classes of Button
    const classesNames = `${style.lm_button} lm_button--${sizeClass} lm_button--${brandClass}`;

    return (
        <button
            className={ classesNames }
            disabled={ disabled || false }
            onClick={ onClick || null }
        >
            <span>{label}</span>
        </button>
    )

};

Button.propTypes = {
    label: PropTypes.string.isRequired,
    size: PropTypes.string,
    brand: PropTypes.string,
    disabled: PropTypes.bool,
    onClick: PropTypes.func
};

export default Button;
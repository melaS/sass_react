import React from 'react';
import Button from '../Button/Button'
import style from './_app.scss';

// Click Button Function
const onClickButton = () => {
    alert('click di prova')
};

// App Component
class App extends React.Component {
    render() {
        return (
            <div className={ style.lm_app }>
                <Button
                    label="demo"
                    brand="crs"
                    size="small"
                />
                <Button
                    label="demo"
                    brand="lmn"
                    size="large"
                    onClick={ onClickButton }
                />
            </div>
        );
    }
}

export default App;
import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App/App'
import './index.scss';

function init() {
    ReactDOM.render(
        <App />,
        document.getElementById('demo')
    )
}

init();
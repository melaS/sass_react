# CSSModules + Sass
Lo Scopo della demo è quello di poter utilizzare CSSModules con Sass.
Uno dei problemi in cui mi sono imbattuto utilizzando l'import dei file **SCSS** nei vari componenti è stato quello di non avere la possibilità di utilizzare le variabile, mixins, functions etc. etc. nei singoli file **SASS**.
Ero costretto, per ogni file **SCSS**, ad importare tutti i file comuni.

Dopo varie ricerche ho trovato un loader che mi permettesse di dichiare la cartella o comunque il percorso dei file comuni a tutti i file **SCSS**.
Il loader in questione si chiama [sass-resources-loader](https://github.com/shakacode/sass-resources-loader).

Questo permette di importare le risorse **SASS** in ogni **modulo SASS**. 

In questo modo non ho avuto più bisogno di importare le variabiles, mixins e functions per ogni Modulo. 

## Istallazione

Si suppone che si dispone già di **Node.JS**.<br/>

Se questo non fosse vero, prima di fare qualsiasi cosa bisogna istallare [Node.JS](https://nodejs.org/it/)

Una volta istallato **Node.JS** bisogna istallare [Yarn](https://yarnpkg.com/en/docs/install#mac-stable).<br/>
Dopo aver istallato sia **Node.js** che **Yarn** basta clonare il repository ed eseguire:

    yarn install

In questo modo verranno scaricati tutti i moduli che serviranno per la compilazione del progetto.

## Uso
***

Ci sono 2 tipi di comandi da poter utilizzare i 2 metodi di build:

* Build Development
* Build Production

***

#### Quindi:

##### Per lo sviluppo:

    yarn dev

##### Per la produzione:

    yarn build
   
## Demo
***

Per la demo ho creato un componente `Button.js` situato nella cartella
    
    src/components/Button
    
In questa cartella troviamo il file `Button.js` e il corrispettivo file **SASS** `_button.scss`.

Nel componente `Button.js` importo il corrispettivo file **SASS** `_button.scss` in questo modo:

    import style from './_button.scss';
    
Cosi facendo ho la possibilità di assegnare le classi presenti nel file **SCSS** direttamente al Componente usando la seguente sintassi:

    style.lm_button
    
 Il file `_button.scss` ha come dipendenze i file presenti nella cartella `src/assets/scss/global`
 
 In questa cartella ho inserito tutti i file comuni agli **SCSS module** come `_functions.scss`, `_mixins.scss`, `_variables.scss` e `_themes.scss`  
 
 #### File `_themes.scss`
 
 Per la demo ho cercato di trattare anche il problema del tema in base al **co-brand**.
 
 Ho creato un file `_temes.scss` dove ho inserito un oggetto **SASS** per definire i vari **colori** realtivi al **co-brand**
 
 Questo perché ci sono delle variabili comuni a tutti i **co-brand** quali `paddin`, `margin`, `grid` che ho definito nel file `_variables.scss` presente nella cartella `src/assets/sass/global`, e variabili che variano in base al **co-brand** come la paletta colori.
 
 Questi ultimi sono stati definiti nell'oggetto presente nel file `_themes.scss` il cui percorso e  `src/assets/sass/global/_themes.scss`.
 
 Per poterli utilizzare ho creto una mixin chiamato `themify` che si trova nel file `src/assets/scsss/global/_minxins.scss`
 
    @mixin themify($themes: $themes, $prefix: $prefix) {
        @each $theme, $map in $themes {
    
            &.#{$prefix}--#{$theme} {
                $theme-map: () !global;
                @each $key, $submap in $map {
                    $value: map-get(map-get($themes, $theme), '#{$key}');
                    $theme-map: map-merge($theme-map, ($key: $value)) !global;
                }
    
                @content;
                $theme-map: null !global;
            }
    
        }
    }
 
Esso ha comme dipendenza una funzione chiamata `themed` presente nel file `src/assets/sass/global/_functions`
 
    @function themed($key) {
        @return map-get($theme-map, $key);
    }
 
Il mixin prende come parametri l'oggetto `$themes` e un **prefisso di classe** `$prefix`.
    
L'oggetto `theme` servirà per poter mappare i vari attributi in base al **co-brand**, mentre il prefisso di classe va a definire il modifier da applicare al componente.

##### Esempio per componente Button
    
     @include themify($themes, 'lm_button') {
        background-color: themed(brandCta);
        color: $color-white;
        ....
      }
      
In questo caso abbiamo un bottone che ha come **Block Component** la classe `.lm_button` e come **Modifier** `.lm_buttons--[co-brand]`.
  
***Questa parte é da considerarsi solo come esempio e non come proposta per la gestione dei **co-brand**.***
 
#### Component `Button.js`
 
Il **Component** `Button.js` prenderà come proprietà i seguenti parametri:
 
 * `label` stringa che va a definire il testo del bottone. (required)
 * `size` il cui valore è una stringa scelta tra: `small`, `medium` e `large` (default: `medium`)
 * `brand` il cui valore è una stringa scelta tra `crs` e `lmn` (default: `lmn`)
 * `disabled` il cui valore è un booleano (default: `false`)
 * `onClick` il cui valore è una funzione (opzionale - default: `null`)
 
Il `size` andrà a definire il **Modifier** per le **dimensioni**. Es. `.lm_button--medium`

Il `brand` andrà a definire il **Modifier** per il **co-brand**. Es. `.lm_button--lmn`

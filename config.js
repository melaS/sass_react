const path = require('path');
const root = path.normalize(path.join(__dirname, ''));

const scssResurces = [
    './src/assets/scss/global/_variables.scss',
    './src/assets/scss/global/_functions.scss',
    './src/assets/scss/global/_mixins.scss',
    './src/assets/scss/global/_themes.scss'
];

const config = {
    ROOT: root,
    HOST: 'localhost',
    PORT: 3000,
    srcPath: path.join(root, './src'),
    buildPath: path.join(root, './build'),
    globalSCSS: scssResurces.map(file => path.join(root, file))
};
module.exports = config;
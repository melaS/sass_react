"use strict";
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const config = require('./config');
const webpackLoaders = require("./webpack.loaders");
const webpackPlugins = require('./webpack.plugins');


module.exports = function (env) {

    const isProd = env === 'production';

    const loaders = webpackLoaders(env);
    const plugins = webpackPlugins(env);

    return {

        devtool: isProd ? 'source-map' : 'eval',

        context: config.srcPath,

        entry: [
            'babel-polyfill',
            'react-hot-loader/patch',
            './index.js',
        ],

        output: {
            path: config.buildPath,
            filename: '[name].[hash].js',
            sourceMapFilename: '[name].map',
            publicPath: isProd ? './' : '/'
        },

        module: {
            rules: loaders
        },

        optimization: {
            minimizer: [
                new UglifyJsPlugin({
                    uglifyOptions: {
                        compress: {
                            drop_console: true,
                        }
                    }
                })
            ]
        },

        devServer: {
            port: config.PORT,
            host: config.HOST,
            contentBase: config.buildPath,
            historyApiFallback: true,
            noInfo: true,
            compress: true,
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
                "Access-Control-Allow-Headers": "X-Requested-With, Content-type, Authorization"
            }
        },

        plugins: plugins
    }


};
